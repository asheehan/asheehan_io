defmodule Asheehan.Repo.Migrations.CreateClimbingSessions do
  use Ecto.Migration

  def change do
    create table(:climbing_sessions) do
      add :location, :text
      add :routes, :map
      add :notes, :text

      timestamps()
    end
  end
end
