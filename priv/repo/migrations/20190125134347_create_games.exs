defmodule Asheehan.Repo.Migrations.CreateGames do
  use Ecto.Migration

  def change do
    create table(:games) do
      add :map, :string
      add :victor, :string
      add :victory_type, :string
      add :blue_queen_id, references(:players, on_delete: :nothing)
      add :blue_checks_id, references(:players, on_delete: :nothing)
      add :blue_skulls_id, references(:players, on_delete: :nothing)
      add :blue_abs_id, references(:players, on_delete: :nothing)
      add :blue_stripes_id, references(:players, on_delete: :nothing)
      add :gold_queen_id, references(:players, on_delete: :nothing)
      add :gold_checks_id, references(:players, on_delete: :nothing)
      add :gold_skulls_id, references(:players, on_delete: :nothing)
      add :gold_abs_id, references(:players, on_delete: :nothing)
      add :gold_stripes_id, references(:players, on_delete: :nothing)

      timestamps()
    end

    create index(:games, [:blue_queen_id])
    create index(:games, [:blue_checks_id])
    create index(:games, [:blue_skulls_id])
    create index(:games, [:blue_abs_id])
    create index(:games, [:blue_stripes_id])
    create index(:games, [:gold_queen_id])
    create index(:games, [:gold_checks_id])
    create index(:games, [:gold_skulls_id])
    create index(:games, [:gold_abs_id])
    create index(:games, [:gold_stripes_id])
  end
end
