defmodule Asheehan.ClimbingTest do
  use Asheehan.DataCase

  alias Asheehan.Climbing

  describe "climbing_sessions" do
    alias Asheehan.Climbing.Session

    @valid_attrs %{location: "some location", notes: "some notes", routes: %{}}
    @update_attrs %{location: "some updated location", notes: "some updated notes", routes: %{}}
    @invalid_attrs %{location: nil, notes: nil, routes: nil}

    def session_fixture(attrs \\ %{}) do
      {:ok, session} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Climbing.create_session()

      session
    end

    test "list_climbing_sessions/0 returns all climbing_sessions" do
      session = session_fixture()
      assert Climbing.list_climbing_sessions() == [session]
    end

    test "get_session!/1 returns the session with given id" do
      session = session_fixture()
      assert Climbing.get_session!(session.id) == session
    end

    test "create_session/1 with valid data creates a session" do
      assert {:ok, %Session{} = session} = Climbing.create_session(@valid_attrs)
      assert session.location == "some location"
      assert session.notes == "some notes"
      assert session.routes == %{}
    end

    test "create_session/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Climbing.create_session(@invalid_attrs)
    end

    test "update_session/2 with valid data updates the session" do
      session = session_fixture()
      assert {:ok, %Session{} = session} = Climbing.update_session(session, @update_attrs)
      assert session.location == "some updated location"
      assert session.notes == "some updated notes"
      assert session.routes == %{}
    end

    test "update_session/2 with invalid data returns error changeset" do
      session = session_fixture()
      assert {:error, %Ecto.Changeset{}} = Climbing.update_session(session, @invalid_attrs)
      assert session == Climbing.get_session!(session.id)
    end

    test "delete_session/1 deletes the session" do
      session = session_fixture()
      assert {:ok, %Session{}} = Climbing.delete_session(session)
      assert_raise Ecto.NoResultsError, fn -> Climbing.get_session!(session.id) end
    end

    test "change_session/1 returns a session changeset" do
      session = session_fixture()
      assert %Ecto.Changeset{} = Climbing.change_session(session)
    end
  end
end
