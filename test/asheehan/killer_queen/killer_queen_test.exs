defmodule Asheehan.KillerQueenTest do
  use Asheehan.DataCase

  alias Asheehan.KillerQueen

  @valid_player_attrs %{name: "some name"}
  def player_fixture(attrs \\ %{}) do
    {:ok, player} =
      attrs
      |> Enum.into(@valid_player_attrs)
      |> KillerQueen.create_player()

    player
  end

  @valid_game_attrs %{
    map: "some map",
    victor: "some victor",
    victory_type: "some victory_type"
  }
  def game_fixture(attrs \\ %{}) do
    {:ok, game} =
      attrs
      |> Enum.into(@valid_game_attrs)
      |> KillerQueen.create_game()

    game
  end

  describe "players" do
    alias Asheehan.KillerQueen.Player

    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    test "list_players/0 returns all players" do
      player = player_fixture()
      assert KillerQueen.list_players() == [player]
    end

    test "get_player!/1 returns the player with given id" do
      player = player_fixture()
      assert KillerQueen.get_player!(player.id) == player
    end

    test "create_player/1 with valid data creates a player" do
      assert {:ok, %Player{} = player} = KillerQueen.create_player(@valid_player_attrs)
      assert player.name == "some name"
    end

    test "create_player/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = KillerQueen.create_player(@invalid_attrs)
    end

    test "update_player/2 with valid data updates the player" do
      player = player_fixture()
      assert {:ok, %Player{} = player} = KillerQueen.update_player(player, @update_attrs)
      assert player.name == "some updated name"
    end

    test "update_player/2 with invalid data returns error changeset" do
      player = player_fixture()
      assert {:error, %Ecto.Changeset{}} = KillerQueen.update_player(player, @invalid_attrs)
      assert player == KillerQueen.get_player!(player.id)
    end

    test "delete_player/1 deletes the player" do
      player = player_fixture()
      assert {:ok, %Player{}} = KillerQueen.delete_player(player)
      assert_raise Ecto.NoResultsError, fn -> KillerQueen.get_player!(player.id) end
    end

    test "change_player/1 returns a player changeset" do
      player = player_fixture()
      assert %Ecto.Changeset{} = KillerQueen.change_player(player)
    end
  end

  describe "games" do
    alias Asheehan.KillerQueen.Game

    @update_attrs %{
      map: "some updated map",
      victor: "some updated victor",
      victory_type: "some updated victory_type"
    }
    @invalid_attrs %{map: nil, recorded_at: nil, victor: nil, victory_type: nil}

    test "list_games/0 returns all games" do
      game = game_fixture()
      assert KillerQueen.list_games() == [game]
    end

    test "get_game!/1 returns the game with given id" do
      game = game_fixture()
      assert KillerQueen.get_game!(game.id) == game
    end

    test "create_game/1 with valid data creates a game" do
      assert {:ok, %Game{} = game} = KillerQueen.create_game(@valid_game_attrs)
      assert game.map == "some map"
      assert game.victor == "some victor"
      assert game.victory_type == "some victory_type"
    end

    test "create_game/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = KillerQueen.create_game(@invalid_attrs)
    end

    test "update_game/2 with valid data updates the game" do
      game = game_fixture()
      assert {:ok, %Game{} = game} = KillerQueen.update_game(game, @update_attrs)
      assert game.map == "some updated map"
      assert game.victor == "some updated victor"
      assert game.victory_type == "some updated victory_type"
    end

    test "update_game/2 with invalid data returns error changeset" do
      game = game_fixture()
      assert {:error, %Ecto.Changeset{}} = KillerQueen.update_game(game, @invalid_attrs)
      assert game == KillerQueen.get_game!(game.id)
    end

    test "delete_game/1 deletes the game" do
      game = game_fixture()
      assert {:ok, %Game{}} = KillerQueen.delete_game(game)
      assert_raise Ecto.NoResultsError, fn -> KillerQueen.get_game!(game.id) end
    end

    test "change_game/1 returns a game changeset" do
      game = game_fixture()
      assert %Ecto.Changeset{} = KillerQueen.change_game(game)
    end
  end

  describe "players and games" do
    alias Asheehan.KillerQueen.Game
    alias Asheehan.KillerQueen.Service.Match

    test "assign_players/1 sets players in a game" do
      Match.set_players([1, 2])
      assert game = KillerQueen.assign_players(game_fixture())
      assert %Game{} = game
      assert game.blue_queen_id !== nil
      assert game.gold_queen_id !== nil
    end
  end
end
