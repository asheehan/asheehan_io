defmodule Asheehan.KillerQueen.Service.MatchTest do
  use ExUnit.Case

  alias Asheehan.KillerQueen.Service.Match

  test "set_players returns players" do
    assert Match.set_players([1, 2, 3]) === [1, 2, 3]
  end

  test "get_players returns the players" do
    Match.set_players([1, 2, 3])
    assert Match.get_players() === [1, 2, 3]
  end

  test "shuffle returns the players" do
    assert Match.set_players([1, 2, 3]) === [1, 2, 3]
    assert players = Match.shuffle()
    assert length(players) === 3
    assert Enum.member?(players, 1)
    assert Enum.member?(players, 2)
    assert Enum.member?(players, 3)
  end
end
