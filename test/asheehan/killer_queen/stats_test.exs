defmodule Asheehan.KillerQueen.StatsTest do
  use Asheehan.DataCase

  alias Asheehan.KillerQueen.Stats

  test "win_percent/2 handles all values" do
    assert Stats.win_percent(10, 0) === nil
    assert Stats.win_percent(10, 101) === 10
  end
end
