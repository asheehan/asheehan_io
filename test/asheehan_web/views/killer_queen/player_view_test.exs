defmodule AsheehanWeb.KillerQueen.PlayerViewTest do
  use Asheehan.DataCase, async: true

  alias AsheehanWeb.KillerQueen.PlayerView
  alias Asheehan.KillerQueen

  @game %{
    map: "some map",
    victor: "some victor",
    victory_type: "some victory_type"
  }
  setup do
    {:ok, player} = KillerQueen.create_player(%{name: "Test"})
    {:ok, _game} = KillerQueen.create_game(@game)

    [player: player]
  end

  test "play_percentage returns the play percentage", c do
    assert KillerQueen.get_player!(c.player.id)
           |> PlayerView.play_percentage()
  end
end
