defmodule AsheehanWeb.KillerQueen.GameViewTest do
  use Asheehan.DataCase, async: true

  alias AsheehanWeb.KillerQueen.GameView
  alias Asheehan.KillerQueen

  setup do
    {:ok, player} = KillerQueen.create_player(%{name: "Test"})

    [player: player]
  end

  test "player_name returns the player's name", c do
    assert GameView.player_name(c.player.id) == c.player.name
  end
end
