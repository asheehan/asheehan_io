defmodule AsheehanWeb.SessionControllerTest do
  use AsheehanWeb.ConnCase

  alias Asheehan.Climbing

  @create_attrs %{location: "block37", notes: "some notes", routes: %{}}
  @update_attrs %{location: "block37", notes: "some updated notes", routes: %{}}
  @invalid_attrs %{location: nil, notes: nil, routes: nil}

  def fixture(:session) do
    {:ok, session} = Climbing.create_session(@create_attrs)
    session
  end

  describe "index" do
    test "lists all climbing_sessions", %{conn: conn} do
      conn = get(conn, Routes.session_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Climbing sessions"
    end
  end

  describe "new session" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.session_path(conn, :new))
      assert html_response(conn, 200) =~ "New Session"
    end
  end

  describe "create session" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.session_path(conn, :create), session: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.session_path(conn, :show, id)

      conn = get(conn, Routes.session_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Session"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.session_path(conn, :create), session: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Session"
    end
  end

  describe "add route to session" do
    setup [:create_session]

    test "adding a route to a session succeeds", c do
      conn =
        post(c.conn, Routes.session_path(c.conn, :create),
          session_id: c.session.id,
          route_type: "V3"
        )

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.session_path(conn, :show, id)

      conn = get(conn, Routes.session_path(conn, :show, id))
      assert html_response(conn, 200) =~ "V3"
    end

    test "incrementing a route in a session succeeds", c do
      conn =
        post(c.conn, Routes.session_path(c.conn, :create),
          session_id: c.session.id,
          route_type: "V3"
        )

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.session_path(c.conn, :show, id)

      conn = post(c.conn, Routes.session_path(conn, :create), session_id: id, route_type: "V3")

      conn = get(conn, Routes.session_path(conn, :show, id))
      assert html_response(conn, 200) =~ "V3 - 2"
    end
  end

  describe "edit session" do
    setup [:create_session]

    test "renders form for editing chosen session", %{conn: conn, session: session} do
      conn = get(conn, Routes.session_path(conn, :edit, session))
      assert html_response(conn, 200) =~ "Edit Session"
    end
  end

  describe "update session" do
    setup [:create_session]

    test "redirects when data is valid", %{conn: conn, session: session} do
      conn = put(conn, Routes.session_path(conn, :update, session), session: @update_attrs)
      assert redirected_to(conn) == Routes.session_path(conn, :show, session)

      conn = get(conn, Routes.session_path(conn, :show, session))
      assert html_response(conn, 200) =~ "block37"
    end

    test "renders errors when data is invalid", %{conn: conn, session: session} do
      conn = put(conn, Routes.session_path(conn, :update, session), session: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Session"
    end
  end

  describe "delete session" do
    setup [:create_session]

    test "deletes chosen session", %{conn: conn, session: session} do
      conn = delete(conn, Routes.session_path(conn, :delete, session))
      assert redirected_to(conn) == Routes.session_path(conn, :index)

      assert_error_sent 404, fn ->
        get(conn, Routes.session_path(conn, :show, session))
      end
    end
  end

  defp create_session(_) do
    session = fixture(:session)
    {:ok, session: session}
  end
end
