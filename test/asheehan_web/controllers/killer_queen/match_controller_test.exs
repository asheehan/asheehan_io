defmodule AsheehanWeb.KillerQueen.MatchControllerTest do
  use AsheehanWeb.ConnCase

  test "index renders ok", c do
    conn = get(c.conn, Routes.match_path(c.conn, :index))
    assert html_response(conn, :ok) =~ "Killer Queen Portal"
  end

  test "new renders ok", c do
    conn = get(c.conn, Routes.match_path(c.conn, :new))
    assert html_response(conn, :ok) =~ "Select Players"
  end

  test "create renders ok", c do
    conn = post(c.conn, Routes.match_path(c.conn, :create), players: ["1", "2"])
    assert redirected_to(conn) == Routes.game_path(conn, :new)
  end
end
