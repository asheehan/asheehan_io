defmodule Asheehan.Repo do
  use Ecto.Repo,
    otp_app: :asheehan,
    adapter: Ecto.Adapters.Postgres
end
