# defmodule Asheehan.Climbing.Service.Session do
#   @moduledoc """
#     Session server for storing routes and notes as the session is running to later be submtted
#   """
#   use GenServer

#   def start_link(default) when is_list(default) do
#     GenServer.start_link(__MODULE__, default, name: __MODULE__)
#   end

#   @impl true
#   def init(_), do: {:ok, %{}}

#   def create(data) do
#     GenServer.cast(__MODULE__, {:init, data})
#   end

#   def add_route(route) do
#     GenServer.cast(__MODULE__, {:add_route, route})
#   end

#   @impl true
#   def handle_cast({:init, data}, _state) do
#     {:noreply, data}
#   end

#   @impl true
#   def handle_cast({:add_route, route}, state) do
#     new_state = put_in(state.routes[route], state.routes[route] + 1)
#     {:noreply, new_state}
#   end
# end
