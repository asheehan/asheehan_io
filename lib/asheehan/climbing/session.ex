defmodule Asheehan.Climbing.Session do
  use Ecto.Schema
  import Ecto.Changeset

  schema "climbing_sessions" do
    field :location, :string
    field :notes, :string
    field :routes, :map, default: %{}

    timestamps()
  end

  @doc false
  def changeset(session, attrs) do
    session
    |> cast(attrs, [:location, :routes, :notes])
    |> validate_required([:location, :routes])
  end

  def locations do
    %{
      "block37" => [:bouldering],
      "uptown" => [:bouldering],
      # "avondale" => [:bouldering, :top_rope, :lead]
      "avondale" => [:bouldering]
    }
  end
end
