defmodule AsheehanWeb.Router do
  use AsheehanWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {AsheehanWeb.LayoutView, :root}

    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  # pipeline :api do
  # plug :accepts, ["json"]
  # end

  scope "/", AsheehanWeb do
    pipe_through :browser

    resources "/images", ImageController
    resources "/climbing", SessionController
    resources "/killer_queen", KillerQueen.MatchController, only: [:index, :new, :create]
    resources "/killer_queen/players", KillerQueen.PlayerController
    resources "/killer_queen/games", KillerQueen.GameController
    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", AsheehanWeb do
  #   pipe_through :api
  # end
end
