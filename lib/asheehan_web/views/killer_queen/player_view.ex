defmodule AsheehanWeb.KillerQueen.PlayerView do
  use AsheehanWeb, :view

  alias Asheehan.KillerQueen
  alias Asheehan.KillerQueen.Stats

  def play_percentage(player) do
    games = Stats.played_games(player.id)
    total_games = KillerQueen.list_games()

    percentage(length(games), length(total_games))
  end

  defp percentage(_num, 0), do: ""
  defp percentage(num, den), do: "#{num / den * 100}%"
end
