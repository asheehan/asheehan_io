defmodule AsheehanWeb.KillerQueen.GameView do
  use AsheehanWeb, :view

  alias Asheehan.KillerQueen

  def player_name(id) do
    KillerQueen.get_player!(id).name
  end
end
