defmodule AsheehanWeb.SessionView do
  use AsheehanWeb, :view

  def possible_routes(:bouldering) do
    ~w(VB V0 V1 V2 V3 V4 V5 V6 V7 V8 V9)
  end

  def possible_routes(type) when type in [:top_rope, :lead] do
    ~w(5.7 5.8 5.9 5.10a 5.10b 5.10c 5.10d 5.11a 5.11b 5.11c 5.11d 5.12a 5.12b 5.12c)
  end
end
