defmodule AsheehanWeb.SessionController do
  use AsheehanWeb, :controller

  alias Asheehan.Climbing
  alias Asheehan.Climbing.Session

  def index(conn, _params) do
    climbing_sessions = Climbing.list_climbing_sessions()
    render(conn, "index.html", climbing_sessions: climbing_sessions)
  end

  def new(conn, _params) do
    changeset = Climbing.change_session(%Session{})
    render(conn, "new.html", changeset: changeset, locations: Session.locations())
  end

  def create(conn, %{"session" => session_params}) do
    case Climbing.create_session(session_params) do
      {:ok, session} ->
        conn
        |> put_flash(:info, "Session created successfully.")
        |> redirect(to: Routes.session_path(conn, :show, session))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset, locations: Session.locations())
    end
  end

  def create(conn, %{"session_id" => id, "route_type" => route}) do
    with session <- Climbing.get_session!(id),
         {:ok, session} <- Climbing.add_route(session, route) do
      conn
      |> put_flash(:info, "Route added successfully.")
      |> redirect(to: Routes.session_path(conn, :show, session))
    end
  end

  def show(conn, %{"id" => id}) do
    session = Climbing.get_session!(id)

    render(conn, "show.html", session: session, route_types: Session.locations()[session.location])
  end

  def edit(conn, %{"id" => id}) do
    session = Climbing.get_session!(id)
    changeset = Climbing.change_session(session)

    render(conn, "edit.html",
      session: session,
      changeset: changeset,
      locations: Session.locations()
    )
  end

  def update(conn, %{"id" => id, "session" => session_params}) do
    session = Climbing.get_session!(id)

    case Climbing.update_session(session, session_params) do
      {:ok, session} ->
        conn
        |> put_flash(:info, "Session updated successfully.")
        |> redirect(to: Routes.session_path(conn, :show, session))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html",
          session: session,
          changeset: changeset,
          locations: Session.locations()
        )
    end
  end

  def delete(conn, %{"id" => id}) do
    session = Climbing.get_session!(id)
    {:ok, _session} = Climbing.delete_session(session)

    conn
    |> put_flash(:info, "Session deleted successfully.")
    |> redirect(to: Routes.session_path(conn, :index))
  end
end
