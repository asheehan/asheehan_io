defmodule AsheehanWeb.KillerQueen.PlayerController do
  use AsheehanWeb, :controller

  alias Asheehan.KillerQueen
  alias Asheehan.KillerQueen.{Player, Stats}

  def index(conn, _params) do
    players =
      Enum.map(KillerQueen.list_players(), fn player ->
        Map.put(player, :stats, Stats.get_stats(player.id))
      end)

    render(conn, "index.html", players: players)
  end

  def new(conn, _params) do
    changeset = KillerQueen.change_player(%Player{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"player" => player_params}) do
    case KillerQueen.create_player(player_params) do
      {:ok, player} ->
        conn
        |> put_flash(:info, "Player created successfully.")
        |> redirect(to: Routes.player_path(conn, :show, player))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    player = KillerQueen.get_player!(id)
    stats = Stats.get_stats(id)
    render(conn, "show.html", player: player, stats: stats)
  end

  def edit(conn, %{"id" => id}) do
    player = KillerQueen.get_player!(id)
    changeset = KillerQueen.change_player(player)
    render(conn, "edit.html", player: player, changeset: changeset)
  end

  def update(conn, %{"id" => id, "player" => player_params}) do
    player = KillerQueen.get_player!(id)

    case KillerQueen.update_player(player, player_params) do
      {:ok, player} ->
        conn
        |> put_flash(:info, "Player updated successfully.")
        |> redirect(to: Routes.player_path(conn, :show, player))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", player: player, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    player = KillerQueen.get_player!(id)
    {:ok, _player} = KillerQueen.delete_player(player)

    conn
    |> put_flash(:info, "Player deleted successfully.")
    |> redirect(to: Routes.player_path(conn, :index))
  end
end
