defmodule AsheehanWeb.KillerQueen.GameController do
  use AsheehanWeb, :controller

  alias Asheehan.KillerQueen
  alias Asheehan.KillerQueen.Game
  alias Asheehan.KillerQueen.Service.Match

  def index(conn, _params) do
    games = KillerQueen.list_games()
    render(conn, "index.html", games: games)
  end

  def new(conn, %{"shuffle" => _}) do
    Match.shuffle()
    redirect(conn, to: Routes.game_path(conn, :new))
  end

  def new(conn, _params) do
    changeset = KillerQueen.new_game(%Game{})
    render(conn, "new.html", changeset: changeset, players: KillerQueen.player_map())
  end

  def create(conn, %{"game" => game_params}) do
    case KillerQueen.create_game(game_params) do
      {:ok, _game} ->
        conn
        |> put_flash(:info, "Game created successfully.")
        |> redirect(to: Routes.game_path(conn, :new))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset, players: KillerQueen.player_map())
    end
  end

  def show(conn, %{"id" => id}) do
    game = KillerQueen.get_game!(id)
    render(conn, "show.html", game: game, players: KillerQueen.player_map())
  end

  def edit(conn, %{"id" => id}) do
    game = KillerQueen.get_game!(id)
    changeset = KillerQueen.change_game(game)
    render(conn, "edit.html", game: game, changeset: changeset, players: KillerQueen.player_map())
  end

  def update(conn, %{"id" => id, "game" => game_params}) do
    game = KillerQueen.get_game!(id)

    case KillerQueen.update_game(game, game_params) do
      {:ok, game} ->
        conn
        |> put_flash(:info, "Game updated successfully.")
        |> redirect(to: Routes.game_path(conn, :show, game))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html",
          game: game,
          changeset: changeset,
          players: KillerQueen.player_map()
        )
    end
  end

  def delete(conn, %{"id" => id}) do
    game = KillerQueen.get_game!(id)
    {:ok, _game} = KillerQueen.delete_game(game)

    conn
    |> put_flash(:info, "Game deleted successfully.")
    |> redirect(to: Routes.game_path(conn, :index))
  end
end
