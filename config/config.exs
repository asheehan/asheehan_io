# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :asheehan,
  ecto_repos: [Asheehan.Repo]

# Configures the endpoint
config :asheehan, AsheehanWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "kh4+lrmYVPkuHJUV4ow3uzbkoA/NcpjSMQukCiJY1an8OSK8liQpr6wdh5vx0PqN",
  live_view: [signing_salt: "qkeNMVHUWwqkK997Qo0zmaIBE0J8JUwG"],
  render_errors: [view: AsheehanWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Asheehan.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Use Slime for templates
config :phoenix, :template_engines,
  slim: PhoenixSlime.Engine,
  slime: PhoenixSlime.Engine

config :phoenix_slime, :use_slim_extension, true

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
